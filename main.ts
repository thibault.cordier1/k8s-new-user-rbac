import { Construct } from 'constructs';
import { App, Chart, ChartProps } from 'cdk8s';
import {  KubeClusterRole, KubeClusterRoleBinding, KubeRole, KubeRoleBinding } from './imports/k8s';


export class MyChart extends Chart {
  constructor(scope: Construct, id: string, props: ChartProps = {}) {
    super(scope, id, props);

    /*
    // define resources here
    new KubeCertificateSigningRequestV1Beta1(this, 'thibault-cordier', {
      metadata: {
        name: 'thibault-cordier'
      },
      spec: {
        groups: [
          "system:authenticated",
          "thibault-cordier"
        ],
        request: "",
        usages: [
          "digital signature",
          "key encipherment",
          "client auth",
          "server auth"
        ]
      }
    })

    */


    const ci_cluster_role = new KubeClusterRole(this, 'ci-cluster-role', {
      rules: [
        {
          apiGroups: [""],
          verbs: ["get", "list", "create", "delete"],
          resources: ["namespaces"]
        }
      ]
    })


    const moodwork_role = new KubeRole(this, 'moodwork-role', {
      metadata: {
        namespace: "moodwork"
      },
      rules: [
        {
          apiGroups: [""],
          verbs: ["*"],
          resources: ["*"]
        }
      ]
    })

    new KubeClusterRoleBinding(this, 'ci-cluster-role-binding', {
      roleRef: {
        kind: "ClusterRole",
        name: ci_cluster_role.name,
        apiGroup: ""
      },
      subjects: [
        {
          kind: "User",
          name: "thibault-cordier",
          apiGroup: ""
        }
      ]
    })

    new KubeRoleBinding(this, 'moodwork-admin-role-binding', {
      metadata: {
        namespace: "moodwork"
      },
      roleRef: {
        kind: "Role",
        name: moodwork_role.name,
        apiGroup: ""
      },
      subjects: [
        {
          kind: "User",
          name: "thibault-cordier",
          apiGroup: ""
        }
      ]
    })

  }
}

const app = new App();
new MyChart(app, 'cdk8s-ci');
app.synth();
