# Ajouter un utilisateur a un cluster

## Créer un certificat

Créer la clé
```
openssl genrsa -out thibault-cordier.key 4096
```
Créer un demande de signature (csr)
Définir un fichier de configuration pour la csr
*csr.cnf*
```
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[ dn ]
CN = thibault-cordier
O = moodwork
[ v3_ext ]
authorityKeyIdentifier=keyid,issuer:always
basicConstraints=CA:FALSE
keyUsage=keyEncipherment,dataEncipherment
extendedKeyUsage=serverAuth,clientAuth
```

```
openssl req -config ./csr.cnf -new -key thibault-cordier.key -nodes -out thibault-cordier.csr
```

Encoder en base64 la csr

```
export BASE64_CSR=$(cat ./thibault-cordier.csr | base64 | tr -d '\n')
```

## Soumettre sa CSR

Créer une CertificateSigningRequest

*csr.yml*
```
apiVersion: "certificates.k8s.io/v1beta1"
kind: "CertificateSigningRequest"
metadata:
  name: "thibault-cordier"
spec:
  groups:
    - "system:authenticated"
    - "thibault-cordier"
  request: ${BASE64_CSR}
  usages:
    - "digital signature"
    - "key encipherment"
    - "client auth"
    - "server auth"
```

Avec le compte administrateur l'envoyer pour signature
```
kubectl apply -f csr.yml
```

## Signer le certificat

```
kubectl certificate approve thibault-cordier
```

## Le récuperer

```
kubectl get csr thibault-cordier -o jsonpath='{.status.certificate}' | base64 -d > thibault-cordier.pem
```


## Configurer Kubectl pour utiliser ces identifiants 

Créer un nouveau credential

```
kubectl config set-credentials 'thibault-cordier' --client-certificate=/home/thibault/thibault-cordier.pem --client-key=/home/thibault/thibault-cordier.key
```

Créer un cluster
```
kubectl config set-cluster k8s-infallible-euler --server=https://XXXXX.api.k8s.fr-par.scw.cloud:6443 --certificate-authority=/home/thibault/cluster-ca.pem
```

Créer un nouveau context
```
kubectl config set-context moodwork-k8s --cluster=k8s-infallible-euler --user='thibault-cordier
```

Utiliser ce context
```
kubectl config use-context moodwork-k8s
```